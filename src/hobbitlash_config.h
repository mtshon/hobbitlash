#ifndef __BITLASH_CONFIG_H__
#define __BITLASH_CONFIG_H__


#ifdef WIN32
	#define WIN32_BUILD 1
#elif defined(__x86_64__) || defined(__i386__)
	#define UNIX_BUILD 1
#elif defined(__SAM3X8E__)
	#define ARM_BUILD 1
#elif (defined(__MK20DX128__) || defined(__MK20DX256__)) && defined (CORE_TEENSY)
	// Teensy 3
	#define ARM_BUILD 2
#elif defined(PART_LM4F120H5QR) //support Energia.nu - Stellaris Launchpad / Tiva C Series 
	#define ARM_BUILD  4 //support Energia.nu - Stellaris Launchpad / Tiva C Series  
#elif defined(ESP8266) || defined(ESP32)
	#define ESP_BUILD 1
#else
	#define AVR_BUILD 1
#endif

#if defined(HIGH) || defined(ARDUINO)		// this detects the Arduino build environment
	#define ARDUINO_BUILD 1
#endif

// Arduino version: 11 - enable by hand if needed; see bitlash-serial.h
//#define ARDUINO_VERSION 11


////////////////////////////////////////////////////
// GLOBAL BUILD OPTIONS
////////////////////////////////////////////////////
//
// Enable LONG_ALIASES to make the parser recognize analogRead(x) as well as ar(x), and so on
// cost: ~200 bytes flash
//#define LONG_ALIASES 1

//
// Enable PARSER_TRACE to make ^T toggle a parser trace debug print stream
// cost: ~400 bytes flash
//#define PARSER_TRACE 1

//
// Enable support for multiple types, such as floats
// costs ~4500 bytes in flash and ~200 bytes in memory on ESP32
#define MULTITYPE_SUPPORT

// Enable support for named variables through useing "var varname;"
#define NAMED_VARS

///////////////////////////////////////////////////////
//
// ARDUINO ETHERNET BUILD OPTIONS
//
///////////////////////////////////////////////////////
//
// Enable WIZ_ETHERNET true to build for telnet access to the official Arduino
// WIZ-5100 Ethernet shield
//#define WIZ_ETHERNET 1
//
// Enable AF_ETHERNET to build for telnet access to the Adafruit Ethernet shield 
// configured per the pinout below
//
//#define AF_ETHERNET 1
//


#endif
