#ifndef __BITLASHVAR_H__
#define __BITLASHVAR_H__

#pragma pack(1)

struct NOVALUE_TYPE
{
	char dummy;
};

static NOVALUE_TYPE NOVALUE = { 0 };

class BITLASHVAR
{
public:
  union
  {
    long int intValue;
    float floatValue;
    char *strValue;
  };
  enum BITLASHVARTYPE { unsetType, intType, floatType, strType };
  BITLASHVARTYPE vType;
  
  BITLASHVAR(){vType= unsetType; intValue=0;}
  
  BITLASHVAR (int n) {intValue=n; vType = intType; }
  BITLASHVAR (long int n) {intValue=n; vType = intType; }
  BITLASHVAR (unsigned int n) {intValue=n; vType = intType; }
  BITLASHVAR (unsigned char n) {intValue=n; vType = intType; }
  BITLASHVAR (long unsigned int n) {intValue=n; vType = intType; }
  BITLASHVAR (float n) {floatValue=n; vType = floatType; }
  BITLASHVAR (double n) {floatValue=(float)n; vType = floatType; }
  BITLASHVAR (char *n) {strValue=n; vType = strType; }
  
  operator int () { if (vType == intType) return intValue; if (vType == floatType) return (int)floatValue; return 0;}
  operator bool () { if (vType == intType) return !!intValue; if (vType == floatType) return floatValue!=0; return 0;}
  operator long int () { if (vType == intType) return intValue; if (vType == floatType) return (long int) floatValue; return 0;}
  operator unsigned int () { if (vType == intType) return intValue; if (vType == floatType) return (unsigned int) floatValue; return 0;}
  operator long unsigned int () { if (vType == intType) return intValue; if (vType == floatType) return (long unsigned int) floatValue; return 0;}
  operator unsigned char () { if (vType == intType) return (unsigned char) intValue; if (vType == floatType) return (unsigned char) floatValue; return 0;}
  operator float () { if (vType == floatType) return floatValue; if (vType == intType) return (float) intValue; return 0; }
  operator double () { if (vType == floatType) return (double) floatValue; if (vType == intType) return (float) intValue; return 0; }
  operator char* () { if (vType == strType) return strValue; return (char*) intValue; }
  
  BITLASHVAR& operator = (NOVALUE_TYPE n) { vType = unsetType; intValue = 0; return *this; }
  BITLASHVAR& operator = (const BITLASHVAR n) {vType=n.vType; intValue=n.intValue; return *this;}
  BITLASHVAR& operator = (const int n) {intValue=n; vType = intType; return *this;}
  BITLASHVAR& operator = (const long int n) {intValue=n; vType = intType; return *this;}
  BITLASHVAR& operator = (const unsigned int n) {intValue=n; vType = intType; return *this;}
  BITLASHVAR& operator = (const long unsigned int n) {intValue=n; vType = intType; return *this;}
  BITLASHVAR& operator = (const unsigned char n) {intValue=n; vType = intType; return *this;}
  BITLASHVAR& operator = (const float n) {floatValue=n; vType = floatType; return *this; }
  BITLASHVAR& operator = (const double n) {floatValue=(float)n; vType = floatType; return *this; }
  BITLASHVAR& operator = (char *n) {strValue=n; vType = strType; return *this; }
  
  BITLASHVAR& operator += (const BITLASHVAR n) {if (n.vType==intType) {*this+=(int)n.intValue;} if (n.vType==floatType) {*this+=(float)n.floatValue;} return *this;}
  BITLASHVAR& operator += (const int n) {if (vType==intType) {intValue+=n;} else if (vType==floatType) floatValue+=n; return *this;}
  BITLASHVAR& operator += (const long int n) {if (vType==intType) {intValue+=n;} else if (vType==floatType) floatValue+=n; return *this;}
  BITLASHVAR& operator += (const unsigned int n) {if (vType==intType) {intValue+=n;} else if (vType==floatType) floatValue+=n; return *this;}
  BITLASHVAR& operator += (const long unsigned int n) {if (vType==intType) {intValue+=n;} else if (vType==floatType) floatValue+=n; return *this;}
  BITLASHVAR& operator += (const unsigned char n) {if (vType==intType) {intValue+=n;} else if (vType==floatType) floatValue+=n; return *this;}
  BITLASHVAR& operator += (const float n) {if (vType==intType) {floatValue=intValue+n; vType=floatType;} else if (vType==floatType) floatValue+=n; return *this;}
  BITLASHVAR& operator += (const double n) {if (vType==intType) {floatValue=intValue+(float)n; vType=floatType;} else if (vType==floatType) floatValue+=(float)n; return *this;}
  BITLASHVAR& operator += (const char *n) { return *this; }

  BITLASHVAR& operator -= (const BITLASHVAR n) {if (n.vType==intType) {*this-=(int)n.intValue;} if (n.vType==floatType) {*this-=(float)n.floatValue;} return *this;}
  BITLASHVAR& operator -= (const int n) {if (vType==intType) {intValue-=n;} else if (vType==floatType) floatValue-=n; return *this;}
  BITLASHVAR& operator -= (const long int n) {if (vType==intType) {intValue-=n;} else if (vType==floatType) floatValue-=n; return *this;}
  BITLASHVAR& operator -= (const unsigned int n) {if (vType==intType) {intValue-=n;} else if (vType==floatType) floatValue-=n; return *this;}
  BITLASHVAR& operator -= (const unsigned char n) {if (vType==intType) {intValue-=n;} else if (vType==floatType) floatValue-=n; return *this;}
  BITLASHVAR& operator -= (const long unsigned int n) {if (vType==intType) {intValue-=n;} else if (vType==floatType) floatValue-=n; return *this;}
  BITLASHVAR& operator -= (const float n) {if (vType==intType) {floatValue=intValue-n; vType=floatType;} else if (vType==floatType) floatValue-=n; return *this;}
  BITLASHVAR& operator -= (const double n) {if (vType==intType) {floatValue=intValue-(float)n; vType=floatType;} else if (vType==floatType) floatValue-=(float)n; return *this;}
  BITLASHVAR& operator -= (const char *n) { return *this; }

  BITLASHVAR& operator ++ () {if (vType==intType) {intValue++;} else if (vType==floatType) floatValue+=1; return *this;}
  BITLASHVAR operator ++ (int) { BITLASHVAR r=*this; if (vType==intType) {intValue++;} else if (vType==floatType) floatValue+=1; return r;}

  BITLASHVAR& operator -- () {if (vType==intType) {intValue--;} else if (vType==floatType) floatValue-=1; return *this;}
  BITLASHVAR operator -- (int) { BITLASHVAR r=*this; if (vType==intType) {intValue--;} else if (vType==floatType) floatValue-=1; return r;}

  BITLASHVAR operator + (const BITLASHVAR n) {BITLASHVAR r = *this; r+=n; return r;}
  BITLASHVAR operator + (const int n) {BITLASHVAR r = *this; r+=n; return r;}
  BITLASHVAR operator + (const long int n) {BITLASHVAR r = *this; r+=n; return r;}
  BITLASHVAR operator + (const unsigned int n) {BITLASHVAR r = *this; r+=n; return r;}
  BITLASHVAR operator + (const unsigned char n) {BITLASHVAR r = *this; r+=n; return r;}
  BITLASHVAR operator + (const long unsigned int n) {BITLASHVAR r = *this; r+=n; return r;}
  BITLASHVAR operator + (const float n) {BITLASHVAR r = *this; r+=n; return r;}
  BITLASHVAR operator + (const double n) {BITLASHVAR r = *this; r+=n; return r;}
  BITLASHVAR operator + (const char *n) {BITLASHVAR r = *this; r+=n; return r;}
  
  BITLASHVAR operator - (const BITLASHVAR n) {BITLASHVAR r = *this; r-=n; return r;}
  BITLASHVAR operator - (const int n) {BITLASHVAR r = *this; r-=n; return r;}
  BITLASHVAR operator - (const long int n) {BITLASHVAR r = *this; r-=n; return r;}
  BITLASHVAR operator - (const unsigned int n) {BITLASHVAR r = *this; r-=n; return r;}
  BITLASHVAR operator - (const unsigned char n) {BITLASHVAR r = *this; r-=n; return r;}
  BITLASHVAR operator - (const long unsigned int n) {BITLASHVAR r = *this; r-=n; return r;}
  BITLASHVAR operator - (const float n) {BITLASHVAR r = *this; r-=n; return r;}
  BITLASHVAR operator - (const double n) {BITLASHVAR r = *this; r-=n; return r;}
  BITLASHVAR operator - (const char *n) {BITLASHVAR r = *this; r-=n; return r;} 

  BITLASHVAR operator * (const BITLASHVAR n) { 
  	if (vType==floatType && n.vType==floatType) return floatValue * n.floatValue; 
  	if (vType==floatType && n.vType==intType) return floatValue * n.intValue; 
  	if (vType==intType && n.vType==floatType) return intValue * n.floatValue; 
  	if (vType==intType && n.vType==intType) return intValue * n.intValue; 
  	return 0; }
  BITLASHVAR operator * (const int n) { if (vType==floatType) return (float)*this * (float)n; return (int)*this * (int)n; }
  BITLASHVAR operator * (const long int n) { if (vType==floatType) return (float)*this * (float)n; return (int)*this * (int)n; }
  BITLASHVAR operator * (const unsigned int n) { if (vType==floatType) return (float)*this * (float)n; return (int)*this * (int)n; }
  BITLASHVAR operator * (const unsigned char n) { if (vType==floatType) return (float)*this * (float)n; return (int)*this * (int)n; }
  BITLASHVAR operator * (const long unsigned int n) { if (vType==floatType) return (float)*this * (float)n; return (int)*this * (int)n; }
  BITLASHVAR operator * (const float n) { return floatValue * (float)n; }
  BITLASHVAR operator * (const double n) { return floatValue * (float)n; }
  BITLASHVAR operator * (const char *n) { return 0; }
  
  BITLASHVAR operator / (const BITLASHVAR n) { 
  	if (vType==floatType && n.vType==floatType) return floatValue / n.floatValue; 
  	if (vType==floatType && n.vType==intType) return floatValue / n.intValue; 
  	if (vType==intType && n.vType==floatType) return intValue / n.floatValue; 
  	if (vType==intType && n.vType==intType) return intValue / n.intValue; 
  	return 0; }
  BITLASHVAR operator / (const int n) { if (vType==floatType) return (float)*this / (float)n; return (int)*this / (int)n; }
  BITLASHVAR operator / (const long int n) { if (vType==floatType) return (float)*this / (float)n; return (int)*this / (int)n; }
  BITLASHVAR operator / (const unsigned int n) { if (vType==floatType) return (float)*this / (float)n; return (int)*this / (int)n; }
  BITLASHVAR operator / (const unsigned char n) { if (vType==floatType) return (float)*this / (float)n; return (int)*this / (int)n; }
  BITLASHVAR operator / (const long unsigned int n) { if (vType==floatType) return (float)*this / (float)n; return (int)*this / (int)n; }
  BITLASHVAR operator / (const float n) { return floatValue / (float)n; }
  BITLASHVAR operator / (const double n) { return floatValue / (float)n; }
  BITLASHVAR operator / (const char *n) { return 0; }

  bool operator == (BITLASHVAR n) { 
  	if (vType==intType && n.vType==intType) return intValue==n.intValue;
  	if (vType==floatType && n.vType==floatType) return floatValue==n.floatValue;
  	if (vType==intType && n.vType==floatType) return intValue==n.floatValue;
  	if (vType==floatType && n.vType==intType) return floatValue==n.intValue;
  	return 0; }
  bool operator == (int n) { if (vType==intType) return intValue==n; else if (vType==floatType) return floatValue==n; else return 0; }
  bool operator == (long int n) { if (vType==intType) return intValue==n; else if (vType==floatType) return floatValue==n; else return 0; }
  bool operator == (unsigned int n) { if (vType==intType) return intValue==n; else if (vType==floatType) return floatValue==n; else return 0; }
  bool operator == (unsigned char n) { if (vType==intType) return intValue==n; else if (vType==floatType) return floatValue==n; else return 0; }
  bool operator == (long unsigned int n) { if (vType==intType) return intValue==n; else if (vType==floatType) return floatValue==n; else return 0; }
  bool operator == (float n) { if (vType==intType) return intValue==n; else if (vType==floatType) return floatValue==n; else return 0; }
  bool operator == (double n) { if (vType==intType) return intValue==n; else if (vType==floatType) return floatValue==n; else return 0; }
  bool operator == (char* n) { return 0; }
  bool operator == (NOVALUE_TYPE n) { return vType==unsetType; }

  bool operator != (BITLASHVAR n) { 
  	if (vType==intType && n.vType==intType) return intValue!=n.intValue;
  	if (vType==floatType && n.vType==floatType) return floatValue!=n.floatValue;
  	if (vType==intType && n.vType==floatType) return intValue!=n.floatValue;
  	if (vType==floatType && n.vType==intType) return floatValue!=n.intValue;
  	return 0; }
  bool operator != (int n) { if (vType==intType) return intValue!=n; else if (vType==floatType) return floatValue!=n; else return 0; }
  bool operator != (long int n) { if (vType==intType) return intValue!=n; else if (vType==floatType) return floatValue!=n; else return 0; }
  bool operator != (unsigned int n) { if (vType==intType) return intValue!=n; else if (vType==floatType) return floatValue!=n; else return 0; }
  bool operator != (unsigned char n) { if (vType==intType) return intValue!=n; else if (vType==floatType) return floatValue!=n; else return 0; }
  bool operator != (long unsigned int n) { if (vType==intType) return intValue!=n; else if (vType==floatType) return floatValue!=n; else return 0; }
  bool operator != (float n) { if (vType==intType) return intValue!=n; else if (vType==floatType) return floatValue!=n; else return 0; }
  bool operator != (double n) { if (vType==intType) return intValue!=n; else if (vType==floatType) return floatValue!=n; else return 0; }
  bool operator != (char* n) { return 0; }

  bool operator < (BITLASHVAR n) { 
  	if (vType==intType && n.vType==intType) return intValue<n.intValue;
  	if (vType==floatType && n.vType==floatType) return floatValue<n.floatValue;
  	if (vType==intType && n.vType==floatType) return intValue<n.floatValue;
  	if (vType==floatType && n.vType==intType) return floatValue<n.intValue;
  	return 0; }
  bool operator < (int n) { if (vType==intType) return intValue<n; else if (vType==floatType) return floatValue<n; else return 0; }
  bool operator < (long int n) { if (vType==intType) return intValue<n; else if (vType==floatType) return floatValue<n; else return 0; }
  bool operator < (unsigned int n) { if (vType==intType) return (unsigned int)intValue<n; else if (vType==floatType) return floatValue<n; else return 0; }
  bool operator < (unsigned char n) { if (vType==intType) return (unsigned char)intValue<n; else if (vType==floatType) return floatValue<n; else return 0; }
  bool operator < (long unsigned int n) { if (vType==intType) return (unsigned long int)intValue<n; else if (vType==floatType) return floatValue<n; else return 0; }
  bool operator < (float n) { if (vType==intType) return intValue<n; else if (vType==floatType) return floatValue<n; else return 0; }
  bool operator < (double n) { if (vType==intType) return intValue<n; else if (vType==floatType) return floatValue<(float)n; else return 0; }
  bool operator < (char* n) { return 0; }

  bool operator > (BITLASHVAR n) { 
  	if (vType==intType && n.vType==intType) return intValue>n.intValue;
  	if (vType==floatType && n.vType==floatType) return floatValue>n.floatValue;
  	if (vType==intType && n.vType==floatType) return intValue>n.floatValue;
  	if (vType==floatType && n.vType==intType) return floatValue>n.intValue;
  	return 0; }
  bool operator > (int n) { if (vType==intType) return intValue>n; else if (vType==floatType) return floatValue>n; else return 0; }
  bool operator > (long int n) { if (vType==intType) return intValue>n; else if (vType==floatType) return floatValue>n; else return 0; }
  bool operator > (unsigned int n) { if (vType==intType) return (unsigned int)intValue>n; else if (vType==floatType) return floatValue>n; else return 0; }
  bool operator > (unsigned char n) { if (vType==intType) return (unsigned char)intValue>n; else if (vType==floatType) return floatValue>n; else return 0; }
  bool operator > (long unsigned int n) { if (vType==intType) return (long unsigned int)intValue>n; else if (vType==floatType) return floatValue>n; else return 0; }
  bool operator > (float n) { if (vType==intType) return intValue>n; else if (vType==floatType) return floatValue>n; else return 0; }
  bool operator > (double n) { if (vType==intType) return intValue>n; else if (vType==floatType) return floatValue>n; else return 0; }
  bool operator > (char* n) { return 0; }

  bool operator <= (BITLASHVAR n) { 
  	if (vType==intType && n.vType==intType) return intValue<=n.intValue;
  	if (vType==floatType && n.vType==floatType) return floatValue<=n.floatValue;
  	if (vType==intType && n.vType==floatType) return intValue<=n.floatValue;
  	if (vType==floatType && n.vType==intType) return floatValue<=n.intValue;
  	return 0; }
  bool operator <= (int n) { if (vType==intType) return intValue<=n; else if (vType==floatType) return floatValue<=n; else return 0; }
  bool operator <= (long int n) { if (vType==intType) return intValue<=n; else if (vType==floatType) return floatValue<=n; else return 0; }
  bool operator <= (unsigned int n) { if (vType==intType) return (unsigned int)intValue<=n; else if (vType==floatType) return floatValue<=n; else return 0; }
  bool operator <= (unsigned char n) { if (vType==intType) return (unsigned char)intValue<=n; else if (vType==floatType) return floatValue<=n; else return 0; }
  bool operator <= (long unsigned int n) { if (vType==intType) return (long unsigned int)intValue<=n; else if (vType==floatType) return floatValue<=n; else return 0; }
  bool operator <= (float n) { if (vType==intType) return intValue<=n; else if (vType==floatType) return floatValue<=n; else return 0; }
  bool operator <= (double n) { if (vType==intType) return intValue<=n; else if (vType==floatType) return floatValue<=n; else return 0; }
  bool operator <= (char* n) { return 0; }

  bool operator >= (BITLASHVAR n) { 
  	if (vType==intType && n.vType==intType) return intValue>=n.intValue;
  	if (vType==floatType && n.vType==floatType) return floatValue>=n.floatValue;
  	if (vType==intType && n.vType==floatType) return intValue>=n.floatValue;
  	if (vType==floatType && n.vType==intType) return floatValue>=n.intValue;
  	return 0; }
  bool operator >= (int n) { if (vType==intType) return intValue>=n; else if (vType==floatType) return floatValue>=n; else return 0; }
  bool operator >= (long int n) { if (vType==intType) return intValue>=n; else if (vType==floatType) return floatValue>=n; else return 0; }
  bool operator >= (unsigned int n) { if (vType==intType) return (unsigned int)intValue>=n; else if (vType==floatType) return floatValue>=n; else return 0; }
  bool operator >= (unsigned char n) { if (vType==intType) return (unsigned char)intValue>=n; else if (vType==floatType) return floatValue>=n; else return 0; }
  bool operator >= (long unsigned int n) { if (vType==intType) return (long unsigned int)intValue>=n; else if (vType==floatType) return floatValue>=n; else return 0; }
  bool operator >= (float n) { if (vType==intType) return intValue>=n; else if (vType==floatType) return floatValue>=n; else return 0; }
  bool operator >= (double n) { if (vType==intType) return intValue>=n; else if (vType==floatType) return floatValue>=n; else return 0; }
  bool operator >= (char* n) { return 0; }
};
#pragma pack()

#endif
