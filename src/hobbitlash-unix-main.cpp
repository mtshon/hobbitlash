/*
	bitlash-unix.c: A minimal implementation of certain core Arduino functions	
	
	The author can be reached at: bill@bitlash.net

	Copyright (C) 2008-2012 Bill Roy

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:
	
	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "hobbitlash_internal.h"

/*

Build:
				cd bitlash/src
	mac:		gcc *.c -o bitlash
	linux:		gcc -pthread *.c -o bitlash

Issues


^C exits instead of STOP *
	signal handler

run scripts from file
	runs one script okay
	mux, which calls itself, fails

foo1 calling foo2 calling foo1

fprintf makes 0-byte file

system() using printf()
	print to buffer

command line options
	-e run command and exit
	-d working directory

full help text
boot segfaults ;)

*/

#define PATH_LEN 256
extern char bitlash_directory[PATH_LEN];
#define DEFAULT_BITLASH_PATH "/.bitlash/"

int main () {

	FILE *shell = popen("echo ~", "r");
	if (!shell) {;}
	int got = fread(&bitlash_directory, 1, PATH_LEN, shell);
	pclose(shell);

	bitlash_directory[strlen(bitlash_directory) - 1] = 0;	// trim /n
	strcat(bitlash_directory, DEFAULT_BITLASH_PATH);

	//sp("Working directory: ");
	//sp(bitlash_directory); speol();

	if (chdir(bitlash_directory) != 0) {
		sp("Cannot enter .bitlash directory.  Does it exist?\n");
	}

	init_fake_eeprom();

	init_millis();
	initBitlash(0);

	//signal(SIGINT, inthandler);
	//signal(SIGKILL, inthandler);

	// run background functions on a separate thread
	pthread_create(&background_thread, NULL, BackgroundMacroThread, 0);

	// run the main stdin command loop
	for (;;) {
		char * ret = fgets(lbuf, STRVALLEN, stdin);
		if (ret == NULL) break;	
		doCommand(lbuf);
		void initlbuf (void);
		initlbuf();
	}
}
